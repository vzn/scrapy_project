# Scrapy
- [Scrapy重点](https://github.com/cvno/scrapy_project/commits/master)
- 自定义扩展(基于信号)
- 自定义去重规则
- 持久化相关 Pipeline
- 不停的去点赞(GET/POST 请求)
- 抽屉首页点赞(起始Url)
- 中间件:爬虫中间件,下载中间件
> 可添加代理
- 自定义命令
```bash
# settings.py > add
COMMANDS_MODULE = "sp3.commands"
# cmd
scrapy crawlall
```
- scrapy 配置
Telnet : 如果运行爬虫,爬虫任务进度,前提是任务没有终止
```bash
telent 127.0.0.1 6023 # 查看爬虫任务进度
est()
```
分布式爬虫改造 (14,15)(16)
- 代理
默认代理
```python
os.environ
{
    http_proxy:http://root:woshiniba@192.168.11.11:9999/
    https_proxy:http://192.168.11.11:9999/
}
```
自定义代码

```python
# 配置文件
DOWNLOADER_MIDDLEWARES = {
   'step8_king.middlewares.ProxyMiddleware': 500,
}
```

```python
import random
import base64
def to_bytes(text, encoding=None, errors='strict'):
    if isinstance(text, bytes):
        return text
    if not isinstance(text, six.string_types):
        raise TypeError('to_bytes must receive a unicode, str or bytes '
                        'object, got %s' % type(text).__name__)
    if encoding is None:
        encoding = 'utf-8'
    return text.encode(encoding, errors)
    
class ProxyMiddleware(object):
    def process_request(self, request, spider):
        PROXIES = [
            {'ip_port': '111.11.228.75:80', 'user_pass': ''},
            {'ip_port': '120.198.243.22:80', 'user_pass': ''},
            {'ip_port': '111.8.60.9:8123', 'user_pass': ''},
            {'ip_port': '101.71.27.120:80', 'user_pass': ''},
            {'ip_port': '122.96.59.104:80', 'user_pass': ''},
            {'ip_port': '122.224.249.122:8088', 'user_pass': ''},
        ]
        proxy = random.choice(PROXIES)
        if proxy['user_pass'] is not None:
            request.meta['proxy'] = to_bytes("http://%s" % proxy['ip_port'])
            encoded_user_pass = base64.encodestring(to_bytes(proxy['user_pass']))
            request.headers['Proxy-Authorization'] = to_bytes('Basic ' + encoded_user_pass)
            print("**************ProxyMiddleware have pass************" + proxy['ip_port'])
        else:
            print("**************ProxyMiddleware no pass************" + proxy['ip_port'])
            request.meta['proxy'] = to_bytes("http://%s" % proxy['ip_port'])
```
