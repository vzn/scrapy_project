

# 自定义扩展


from scrapy import signals


class MyExtension(object):
    def __init__(self, value):
        self.value = value

    @classmethod    # 类方法
    def from_crawler(cls, crawler):
        val = crawler.settings.getint('MMMM')
        ext = cls(val)

        # 在scrapy中注册信号： spider_opened   | ext.opened 触发信号时执行的函数
        crawler.signals.connect(ext.opened, signal=signals.spider_opened)   # < spider_opened
        # 在scrapy中注册信号： spider_closed
        crawler.signals.connect(ext.closed, signal=signals.spider_closed)

        return ext

    def opened(self, spider):
        print('open')

    def closed(self, spider):
        print('close')