# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class Sp2Pipeline(object):
    '''注册后才可以用'''
    def process_item(self, item, spider):
        '''

        :param item: 爬虫中yield回来的对象
        :param spider: 爬虫对象 obj = JiandanSpider()
        :return:
        '''
        # 可以应用在某个爬虫,而不是全部, 这个文件的类是全局的   # TODO
        if spider.name == 'jiadnan':
            pass

        print(item)
        # TODO
        # 将item传递给下一个pipeline的process_item方法
        return item
        # from scrapy.exceptions import DropItem
        # raise DropItem()  下一个pipeline的process_item方法不在执行

    @classmethod
    def from_crawler(cls, crawler):
        """
        初始化时候，用于创建pipeline对象
        :param crawler:
        :return:
        """
        # TODO
        # val = crawler.settings.getint('MMMM')   # 获取这个值并 int 去配置文件读取   可以是 mysql 连接数据
        val = crawler.settings.get('MMMM')   # 去配置文件读取   可以是 mysql 连接数据
        # return cls(val)
        return cls()

    def open_spider(self,spider):
        """
        爬虫开始执行时，调用
        :param spider:
        :return:
        """
        print('000000')

    def close_spider(self,spider):
        """
        爬虫关闭时，被调用
        :param spider:
        :return:
        """
        print('111111')


class Sp3Pipeline(object):
    '''注册后才可以用'''
    def process_item(self, item, spider):
        '''

        :param item: 爬虫中yield回来的对象
        :param spider: 爬虫对象 obj = JiandanSpider()
        :return:
        '''
        print(item)
        return item

    @classmethod
    def from_crawler(cls, crawler):
        """
        初始化时候，用于创建pipeline对象
        :param crawler:
        :return:
        """
        # val = crawler.settings.getint('MMMM')   # 获取这个值并 int 去配置文件读取   可以是 mysql 连接数据
        val = crawler.settings.get('MMMM')   # 去配置文件读取   可以是 mysql 连接数据
        # return cls(val)
        return cls()

    def open_spider(self,spider):
        """
        爬虫开始执行时，调用
        :param spider:
        :return:
        """
        print('000000')

    def close_spider(self,spider):
        """
        爬虫关闭时，被调用
        :param spider:
        :return:
        """
        print('111111')

# TODO

'''
检测 CustomPipeline类中是否有 from_crawler方法
如果有：
       obj = 类.from_crawler()
如果没有：
       obj = 类()
obj.open_spider()

while True:
    爬虫运行，并且执行parse各种各样的方法，yield item
    obj.process_item()

obj.close_spider()    
'''

# 自定义 pipline 方法
from scrapy.exceptions import DropItem

class CustomPipeline(object):
    def __init__(self,v):
        self.value = v

    def process_item(self, item, spider):
        # 操作并进行持久化

        # return表示会被后续的pipeline继续处理
        return item

        # 表示将item丢弃，不会被后续pipeline处理
        # raise DropItem()


    @classmethod
    def from_crawler(cls, crawler):
        """
        初始化时候，用于创建pipeline对象
        :param crawler:
        :return:
        """
        # val = crawler.settings.getint('MMMM')   # 获取这个值并 int 去配置文件读取   可以是 mysql 连接数据
        val = crawler.settings.get('MMMM')   # 去配置文件读取   可以是 mysql 连接数据
        return cls(val)
        # return cls()

    def open_spider(self,spider):
        """
        爬虫开始执行时，调用
        :param spider:
        :return:
        """
        print('000000')

    def close_spider(self,spider):
        """
        爬虫关闭时，被调用
        :param spider:
        :return:
        """
        print('111111')