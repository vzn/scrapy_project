# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.selector import Selector

'''
1. 发送一个GET请求,抽屉
   获取cookie
   
2. 用户密码POST登录：携带上一次cookie
   返回值：9999
   
3. 为所欲为，携带cookie，点赞
'''


class ChoutiSpider(scrapy.Spider):
    name = "chouti"
    allowed_domains = ["chouti.com"]
    start_urls = ['http://chouti.com/']
    cookie_dict = {}

    # 起始位置 : 自定义这个方法
    def start_requests(self):
        '''每创建一个爬虫都会执行这个方法,如果自己没有就去父类那儿'''
        for url in self.start_urls:
            # 指定回调函数
            # 放到调度器里面
            yield Request(url, dont_filter=True, callback=self.login)  # 执行回调函数

    def login(self, response):
        # response.text  首页内容
        from scrapy.http.cookies import CookieJar
        cookie_jar = CookieJar()  # 对象 里面封装了cookie
        cookie_jar.extract_cookies(response, response.request)  # 去响应中提取cookie

        # 把cookie转成 字典
        for k, v in cookie_jar._cookies.items():
            for i, j in v.items():
                for m, n in j.items():
                    self.cookie_dict[m] = n.value
        # 帐号密码
        post_dict = {
            'phone': '8615038273660',
            'password': 'qweasd',
            'oneMonth': '1',
        }

        # 把字典转成 http 请求的 data       Request > body 字段
        import urllib.parse


        # 目的: 发送POST请求进行登录, > ? 但是这里并没有开始动作
        yield Request(
            url='http://dig.chouti.com/login',
            method='POST',
            cookies=self.cookie_dict,
            body=urllib.parse.urlencode(post_dict),
            headers={'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            callback=self.get_news,
        )

    def get_news(self, response):
        '''登录'''
        print(response.text)
        # 获取新闻列表
        yield Request(
            url='http://dig.chouti.com/',
            method='GET',
            cookies=self.cookie_dict,
            callback=self.run,
        )

    def run(self, response):
        '''点赞'''
        base_url = 'http://dig.chouti.com/link/vote?linksId='
        # 找news id
        hxs = Selector(response)
        link_id_list = hxs.xpath('//div[@class="part2"]/@share-linkid').extract()
        print(link_id_list)

        # 点赞
        for id in link_id_list:
            url = ''.join((base_url, id))
            yield Request(url=url, method='POST', cookies=self.cookie_dict, callback=self.res)

        # page 页码
        # page_list = hxs.xpath('//div[@id="dig_lcpage"]//a/@href').extract()
        # page_base_url = 'http://dig.chouti.com'
        # for page in page_list:
        #     url = ''.join((page_base_url,page))
        #     print(url)
        #     yield Request(url=url,method='GET',callback=self.run)


    def res(self, response):
        '''执行结果'''
        print(response.text,)
