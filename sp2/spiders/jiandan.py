# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector
from scrapy.http import Request

class JiandanSpider(scrapy.Spider):
    name = 'jiandan'
    allowed_domains = ['jiandan.net']
    start_urls = ['http://jiandan.net/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url,dont_filter=True,callback=self.parse)

    def parse(self, response):
        hxs = Selector(response)
        a_list = hxs.xpath('//div[@class="indexs"]/h2').extract()
        # 持久化?  # TODO
        for tag in a_list:
            url = tag.xpath('./a/@href').extract()
            text = tag.xpath('./a/text()').extract()
            from ..items import Sp2Item     # Sp2Item 结构化   # TODO
            yield Sp2Item(url=url,text=text)

